# Angular6Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

To Do List : 

1). Form Name already exists (done).
2). Save response get proper (done).
3). On Header Save check (done).
4). Combobox and checkbox field work (done).
5). On update field type change but field name also get updateing (Wrong) (done).
6). Universion Reset along with save (done).
7). Field name already exists (done).
8). DepartmentId send with save (done).
9). Delete button work (done).
10). On save button click data again save here should get updated.
11). Transaction in Nodejs on error should pass to user and roll back.
