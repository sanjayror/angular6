import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  AbstractControl,
  EmailValidator,
  FormArray
} from '@angular/forms';
import { CustomValidators } from '../shared/custom.validators';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from './employee.service';
import { IEmployee } from './IEmployee';
import { ISkill } from './ISkill';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css'],
  providers: [EmployeeService]
})
export class CreateEmployeeComponent implements OnInit {
  employeeForm_OldMethod: FormGroup;
  employeeForm: FormGroup;
  employee: IEmployee;
  pageTitle: string;
  fullNameLength = 0;

  validationMessages = {
    fullName: {
      required: 'fullName is required.',
      minlength: 'fullName is must be greater then 2 charcters.',
      maxlength: 'fullName must be less then 10 charcters.'
    },
    email: {
      required: 'email is required',
      emailDomain: 'Email domain should be pragimtech.com'
    },
    confirmEmail: {
      required: 'confirm email is required',
      emailDomain: 'confirm Email domain should be pragimtech.com'
    },
    emailGroup: {
      emailMismatch: 'email and confirm email not match'
    },
    phone: {
      required: 'phone is required'
    },
    skillName: {
      required: 'skillName is required.'
    },
    experienceInYears: {
      required: 'experience is required'
    },
    proficiency: {
      required: 'proficiency is required'
    }
  };

  formErrors = {
    fullName: '',
    email: '',
    confirmEmail: '',
    emailGroup: '',
    skillName: '',
    phone: '',
    contactPreference: '',
    experienceInYears: '',
    proficiency: ''
  };

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private router: Router) { }

  ngOnInit() {
    this.employeeForm_OldMethod = new FormGroup({
      fullName: new FormControl(),
      email: new FormControl(),
      skills: new FormGroup({
        skillName: new FormControl(),
        experienceInYears: new FormControl(),
        proficiency: new FormControl()
      })
    });

    this.employeeForm = this.fb.group({
      fullName: [
        '',
        [Validators.required, Validators.minLength(2), Validators.maxLength(10)]
      ],
      emailGroup: this.fb.group(
        {
          email: [
            '',
            [Validators.required, CustomValidators.emailDomain('pragimtech.com')]
          ],
          confirmEmail: ['', Validators.required]
        },
        { validator: matchEmail }
      ),
      phone: [''],
      contactPreference: ['email'],
      skills: this.fb.array([this.addSkillFormGroup()])
    });

    this.employeeForm
      .get('contactPreference')
      .valueChanges.subscribe((data: string) => {
        this.onContactPreferenceChange(data);
      });

    this.route.paramMap.subscribe((params) => {
      const empId = +params.get('id');
      if (empId) {
        this.pageTitle = 'Edit Employee';
        this.getEmployee(empId);
      } else {
        this.pageTitle = 'Create Employee';
        this.employee = {
          id: null,
          fullName: '',
          contactPreference: '',
          email: '',
          phone: null,
          skills: []
        };
      }
    });

    // this.employeeForm.controls.fullName.setValue('A');

    // this.employeeForm.get('fullName').valueChanges.subscribe(
    //   (value: string) => {
    //     this.fullNameLength = value.length;
    //   });

    // this.employeeForm.get('skills').valueChanges.  here only innter block (Nested) will log to server
    this.employeeForm.valueChanges.subscribe((value: any) => {
      console.log(JSON.stringify(value));
      this.logValidationErrors(this.employeeForm);
    });
  }

  getEmployee(id: number) {
    this.employeeService.getEmployee(id).subscribe((employee: IEmployee) => {
      this.editEmployee(employee);
      this.employee = employee;
    }
      , (error) => console.log(error)
    );
  }

  editEmployee(employee: IEmployee) {
    this.employeeForm.patchValue({
      fullName: employee.fullName,
      contactPreference: employee.contactPreference,
      emailGroup: {
        email: employee.email,
        confirmEmail: employee.email
      },
      phone: employee.phone
    });

    this.employeeForm.setControl('skills', this.setExistingSkills(employee.skills));
  }

  setExistingSkills(skillSets: ISkill[]): FormArray {
    const formArray = new FormArray([]);

    skillSets.forEach(s => {
      formArray.push(this.fb.group({
        skillName: s.skillName,
        experienceInYears: s.experienceInYears,
        proficiency: s.proficiency,
      }));
    });
    return formArray;
  }

  addSkillButtonClick(): void {
    (this.employeeForm.get('skills') as FormArray).controls.push(
      this.addSkillFormGroup()
    );
  }

  getFormControl() {
    return (this.employeeForm.get('skills') as FormArray).controls;
  }

  addSkillFormGroup(): FormGroup {
    return this.fb.group({
      skillName: ['', Validators.required],
      experienceInYears: ['', Validators.required],
      proficiency: ['', Validators.required]
    });
  }

  removeSkillButtonClick(skillGroupIndex: number): void {
    // (this.employeeForm.get('skills') as FormArray).removeAt(skillGroupIndex);
    const skillsFormArray = (this.employeeForm.get('skills') as FormArray);
    skillsFormArray.removeAt(skillGroupIndex);
    skillsFormArray.markAsDirty();
    skillsFormArray.markAsTouched();
  }

  onContactPreferenceChange(selectedValue: string): void {
    const phoneControl = this.employeeForm.get('phone');
    if (selectedValue === 'phone') {
      phoneControl.setValidators(Validators.required);
    } else {
      phoneControl.clearValidators();
    }
    phoneControl.updateValueAndValidity();
  }

  onSubmit(): void {
    console.log(this.employeeForm);
    console.log(this.employeeForm.controls.fullName.value);
    // console.log(this.employeeForm.get('fullName').value);

    this.mapEmployeeValuesToEmployeeModel();
    if (this.employee.id) {
      this.employeeService.updateEmployee(this.employee).subscribe(() =>
        this.router.navigate(['employees']),
        (error: any) => console.log(error)
      );
    } else {
      this.employeeService.addEmployee(this.employee).subscribe(() =>
        this.router.navigate(['employees']),
        (error: any) => console.log(error)
      );
    }
  }

  mapEmployeeValuesToEmployeeModel() {
    this.employee.fullName = this.employeeForm.value.fullName;
    this.employee.contactPreference = this.employeeForm.value.contactPreference;
    this.employee.email = this.employeeForm.value.emailGroup.email;
    this.employee.phone = this.employeeForm.value.phone;
    this.employee.skills = this.employeeForm.value.skills;
  }

  logValidationErrors(group: FormGroup = this.employeeForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);

      // console.log(`Key : ${key}, Value : ${abstractControl.value}`);
      // abstractControl.disable();

      this.formErrors[key] = '';
      if (
        abstractControl &&
        !abstractControl.valid &&
        (abstractControl.touched || abstractControl.dirty ||
          abstractControl.value !== '')
      ) {
        const messages = this.validationMessages[key];

        for (const errorKey in abstractControl.errors) {
          if (errorKey) {
            this.formErrors[key] += messages[errorKey] + ' ';
          }
        }
      }

      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      }

      // if (abstractControl instanceof FormArray) {
      //   for (const controls of abstractControl.controls) {
      //     if (controls instanceof FormGroup) {
      //       this.logValidationErrors(controls);
      //     }
      //   }
      // }
    });
  }

  onLoadDataClick(): void {
    // this.employeeForm.patchValue({}); // Use to update All or subset of from controls.

    // this.employeeForm.setValue({
    //   fullName: 'PT', email: 'test@gmail.com',
    //   skills: { skillName: 'Skill', experienceInYears: '1', proficiency: 'intermediate' }
    // });

    // this.logValidationErrors(this.employeeForm);
    // console.log(this.formErrors);

    const formArray = new FormArray([
      new FormControl('john', Validators.required),
      new FormGroup({
        country: new FormControl('', Validators.required)
      }),
      new FormArray([])
    ]);

    const formArray1 = this.fb.array([
      // Seralize as arrays
      new FormControl('john', Validators.required),
      new FormControl('IT', Validators.required),
      new FormControl('Male', Validators.required)
    ]);
    formArray1.push(new FormControl('john', Validators.required));

    const formGroup = this.fb.group([
      // Seralize as object
      new FormControl('john', Validators.required),
      new FormControl('IT', Validators.required),
      new FormControl('Male', Validators.required)
    ]);

    console.log(formArray);
    console.log(formGroup);

    console.log(formArray.length);
    for (const control of formArray.controls) {
      if (control instanceof FormControl) {
        console.log('Control is FormControl');
      }
      if (control instanceof FormGroup) {
        console.log('Control is FormGroup');
      }
      if (control instanceof FormArray) {
        console.log('Control is FormArray');
      }
    }
  }
}

// function emailDomain(domainName: string) {
//   return (control: AbstractControl): { [key: string]: any } | null => {
//     const email: string = control.value;
//     const domain = email.substring(email.lastIndexOf('@') + 1);
//     if (domain === '' || domain.toLowerCase() === domainName.toLowerCase()) {
//       return null;
//     } else {
//       return { 'emailDomain': true };
//     }
//   };
// }

function matchEmail(group: AbstractControl): { [key: string]: any } | null {
  const emailControl = group.get('email');
  const confirmEmailControl = group.get('confirmEmail');

  if (
    emailControl.value.toLowerCase() ===
    confirmEmailControl.value.toLowerCase() ||
    confirmEmailControl.pristine && confirmEmailControl.value === ''
  ) {
    return null;
  } else {
    return { emailMismatch: true };
  }
}
