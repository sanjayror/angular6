import { Component, OnInit, ViewChild } from '@angular/core';
import { pie3d, pareto2d } from './chartData';

@Component({
  selector: 'app-chartv2',
  templateUrl: './chartv2.component.html',
  styleUrls: ['./chartv2.component.css']
})
export class Chartv2Component implements OnInit {

  // *********************************  (Integration) *********************************************//
  // https://www.fusioncharts.com/
  // https://www.fusioncharts.com/angular2-js-charts
  // npm install angular-fusioncharts --save
  // npm install fusioncharts --save

  // Import angular-fusioncharts
  // import { FusionChartsModule } from 'angular-fusioncharts';

  // // Import FusionCharts library
  // import * as FusionCharts from 'fusioncharts';

  // // Load FusionCharts Individual Charts
  // import * as Charts from 'fusioncharts/fusioncharts.charts';

  // // Use fcRoot function to inject FusionCharts library, and the modules you want to use
  // FusionChartsModule.fcRoot(FusionCharts, Charts)

  // FusionChartsModule // Include in imports
  // ***********************************  (Integration) *******************************************//

  ////////////////////////////////////// For Other charts //////////////////////////////////////////
  width = 600;
  height = 400;
  type = 'pie3d';
  dataFormat = 'json';
  dataSources = pie3d;

  typepareto2d = 'pareto2d';
  dataSourcespareto2d = pareto2d;
  ////////////////////////////////////// For Other charts /////////////////////////////////////////////

  data: Object;
  dataSource: Object;
  chartConfig: Object;

  // --------------------------------- gauge ----------------------------------------------------------
  // npm install --save ngx-gauge
  gaugeType = 'semi';
  gaugeValue = 28.3;
  gaugeLabel = 'Speed';
  gaugeAppendText = 'km/hr';
  SelectedValue = 95;

  thresholdConfig = {
    '0': { color: 'green' },
    '40': { color: 'orange' },
    '75.5': { color: 'red' }
  };
  // --------------------------------- gauge ------------------------------------------------------------

  // --------------------------------- angular-gauge-chart ----------------------------------------------
  // npm install --save angular-gauge-chart

  public canvasWidth = 300;
  public needleValue = 5;
  public centralLabel = '';
  public name = 'Gauge chart';
  public bottomLabel = '5';

  public options = {
    hasNeedle: true,
    needleColor: 'black', // gray
    needleUpdateSpeed: 1500,
    arcColors: ['rgb(255,84,84)', 'rgb(239,214,19)', 'rgb(61,204,91)'], // ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [40, 60], // [30],
    rangeLabel: ['0', '100'], // ['52', '8'],
    needleStartValue: 50,
  };
  // --------------------------------- angular-gauge-chart ----------------------------------------------

  constructor() {
    this.chartConfig = {
      width: '700',
      height: '400',
      type: 'column2d',
      dataFormat: 'json',
    };

    this.dataSource = {
      'chart': {
        'caption': 'Countries With Most Oil Reserves [2017-18]',
        'subCaption': 'In MMbbl = One Million barrels',
        'xAxisName': 'Country',
        'yAxisName': 'Reserves (MMbbl)',
        'numberSuffix': 'K',
        'theme': 'fusion',
      },
      'data': [{
        'label': 'Venezuela',
        'value': '290'
      }, {
        'label': 'Saudi',
        'value': '260'
      }, {
        'label': 'Canada',
        'value': '180'
      }, {
        'label': 'Iran',
        'value': '140'
      }, {
        'label': 'Russia',
        'value': '115'
      }, {
        'label': 'UAE',
        'value': '100'
      }, {
        'label': 'US',
        'value': '30'
      }, {
        'label': 'China',
        'value': '30'
      }]
    };

  }

  fnSetValue(): void {
    const bar = Math.floor(Math.random() * 101);

    this.gaugeValue = this.SelectedValue;
    this.needleValue = this.SelectedValue;
    this.bottomLabel = this.SelectedValue.toString();
  }

  fnSetRandomNumber(): void {
    const bar = Math.floor(Math.random() * 101);

    this.gaugeValue = bar;
    this.needleValue = bar;
    this.bottomLabel = bar.toString() + ' km/hr';
  }

  ngOnInit() {
  }

}
