export const pie3d = {
    'chart': {
        'caption': 'Recommended Portfolio Split',
        'subcaption': 'For a net-worth of $1M',
        'showvalues': '1',
        'showpercentintooltip': '0',
        'numberprefix': '$',
        'enablemultislicing': '1',
        'theme': 'fusion'
    },
    'data': [
        {
            'label': 'Equity',
            'value': '300000'
        },
        {
            'label': 'Debt',
            'value': '230000'
        },
        {
            'label': 'Bullion',
            'value': '180000'
        },
        {
            'label': 'Real-estate',
            'value': '270000'
        },
        {
            'label': 'Insurance',
            'value': '20000'
        }
    ]
};

export const pareto2d = {
    'chart': {
        'caption': 'Late arrivals by reported cause',
        'subcaption': 'Last month',
        'pyaxisname': 'No. of Occurrence',
        'theme': 'fusion',
        'showsecondarylimits': '0',
        'showdivlinesecondaryvalue': '0',
        'plottooltext': 'Due to $label, late arrivals count is : <b>$dataValue</b> of the total <b>$sum</b> employees',
        'drawcrossline': '1'
    },
    'data': [
        {
            'label': 'Traffic',
            'value': '5680'
        },
        {
            'label': 'Family Engagement',
            'value': '1036'
        },
        {
            'label': 'Public Transport',
            'value': '950'
        },
        {
            'label': 'Weather',
            'value': '500'
        },
        {
            'label': 'Emergency',
            'value': '140'
        },
        {
            'label': 'Others',
            'value': '68'
        }
    ]
};
