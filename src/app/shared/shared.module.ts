import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularToasterComponent } from '../AngularToaster/angular-toaster.component';
@NgModule({
  declarations: [AngularToasterComponent],
  imports: [
    // CommonModule (Currenlty not needed (When If and For use in program then we will use it))
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    AngularToasterComponent,
  ]
})
export class SharedModule { }
