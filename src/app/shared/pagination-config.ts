import { Component } from '@angular/core';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-pagination-config',
  template: `<p>This pagination uses custom default values</p>
             <ngb-pagination [collectionSize]="70" [(page)]="page" (pageChange)="onPager($event)"></ngb-pagination>
            `,
  providers: [NgbPaginationConfig] // add NgbPaginationConfig to the component providers
})
// tslint:disable-next-line:component-class-suffix
export class NgbdPaginationConfig {
  page = 4;

  onPager(event: number): void {
    console.log(`Pager event Is:  ${event}`);
  }

  constructor(config: NgbPaginationConfig) {
    // customize default values of paginations used by this component tree
    config.size = 'sm';
    config.boundaryLinks = true;
  }
}
