import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules, NoPreloading } from '@angular/router';
import { FormMasterComponent } from './customForm/form-master.component';
import { ChartComponent } from './chart/chart.component';
import { Chartv2Component } from './chartv2/chartv2.component';
import { HomeComponent } from './home.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { ToastrComponent } from './toastr/toastr.component';
import { CustomPreloadingService } from './custom-preloading.service';
import { Ng6featuresComponent } from './ng6features/ng6features.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'formMaster', component: FormMasterComponent },
  { path: 'chart', component: ChartComponent },
  { path: 'chart2', component: Chartv2Component },
  { path: 'toastr', component: ToastrComponent },
  { path: 'ng6features', component: Ng6featuresComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'employees', data: { preload: true }, loadChildren: './employee/employee.module#EmployeeModule' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: CustomPreloadingService })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
