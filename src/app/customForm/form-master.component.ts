import { Component, OnInit } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CustomFormService } from '../service/customForm.Service';

@Component({
  selector: 'app-form-master',
  templateUrl: './form-master.component.html',
  styleUrls: ['./form-master.component.css'],
  providers: []
})
export class FormMasterComponent implements OnInit {
  FormMasterId = 0;
  FormDetailId = 0;

  btnAddText = 'Add';
  Abbreviation = '';
  SubFieldName: string;
  SubfieldData = [];

  formname: string;
  fieldname: string;
  position: string;
  required: string;

  fieldJson = [];
  finalData = [];
  formData = [];
  fieldData = [];

  SelectedDepartement: string;
  SelectedFieldType: string;

  data;

  departmentList: IDepartment[] = [
    { value: '1', text: 'IT' },
    { value: '2', text: 'Sales' },
    { value: '3', text: 'HR' }
  ];

  fieldTypeList: IDataType[] = [
    { value: '1', abbreviation: 'TEXT', text: 'Text' },
    { value: '2', abbreviation: 'COMBOBOX', text: 'Combobox' },
    { value: '3', abbreviation: 'CHECKBOX', text: 'checkbox' },
    { value: '4', abbreviation: 'NUMBER', text: 'number' },
  ];

  constructor(public toastr: ToastrManager,
    private _CustomFormService: CustomFormService) { }

  ngOnInit() {
    this.fnGetData();
  }

  fnGetData() {
    this._CustomFormService.GetData().subscribe(empList => {
      if (empList.recordset.length > 0) {
        this.data = empList.recordset;

        const result = [];
        const map = new Map();

        for (const item of empList.recordset) {
          if (!map.has(item.FormMasterId)) {
            map.set(item.FormMasterId, true);    // set any value to Map
            result.push({
              FormMasterId: item.FormMasterId,
              name: item.FormName
            });
          }
        }
        this.formData = result;

      }
    });
  }

  fnAdd() {
    if (this.SelectedDepartement === undefined || this.SelectedDepartement === null || this.SelectedDepartement === '') {
      this.toastr.warningToastr('Please select department', 'Alert!');
      return false;
    }

    if (this.SelectedFieldType === undefined || this.SelectedFieldType === null || this.SelectedFieldType === '') {
      this.toastr.warningToastr('Please select field type', 'Alert!');
      return false;
    }

    if (this.fieldname === undefined || this.fieldname === null || this.fieldname === '') {
      this.toastr.warningToastr('Please add field name', 'Alert!');
      return false;
    }

    if (this.position === undefined || this.position === null || this.position === '') {
      this.toastr.warningToastr('Please select position', 'Alert!');
      return false;
    }

    if (this.required === undefined || this.required === null || this.required === '') {
      this.toastr.warningToastr('Please select required', 'Alert!');
      return false;
    }

    for (let iRows = 0; iRows < this.fieldData.length; iRows++) {
      if (this.fieldData[iRows].FieldName.toLowerCase() === this.fieldname.toLowerCase() &&
        this.fieldData[iRows].FormDetailId !== this.FormDetailId) {
        this.toastr.warningToastr('Field Name already exists.', 'Alert!');
        return false;
      }
    }

    if (this.FormDetailId === 0) {
      const fieldJson = {
        FormMasterId: this.FormMasterId, // needed here or not have to take a look..
        FormDetailId: -1,
        FieldName: this.fieldname,
        FieldType: this.SelectedFieldType,
        Required: this.required,
        Position: this.position,
        DepartmentId: this.SelectedDepartement,
        DepartmentName: this.departmentList.filter(t => t.value === this.SelectedDepartement)[0]['text'],
        FieldTypeText: this.fieldTypeList.filter(t => t.value === this.SelectedFieldType)[0]['text'],
        FieldData: ''
      };

      this.fieldData.push(fieldJson);
    } else {
      for (let iRows = 0; iRows < this.fieldData.length; iRows++) {
        if (this.fieldData[iRows].FormDetailId === this.FormDetailId) {
          this.fieldData[iRows].FieldName = this.fieldname;
          this.fieldData[iRows].FieldType = this.SelectedFieldType;
          this.fieldData[iRows].Required = this.required;
          this.fieldData[iRows].Position = this.position;
          this.fieldData[iRows].DepartmentId = this.SelectedDepartement;
          this.fieldData[iRows].DepartmentName = this.departmentList.filter(t => t.value === this.SelectedDepartement)[0]['text'];
          this.fieldData[iRows].FieldTypeText = this.fieldTypeList.filter(t => t.value === this.SelectedFieldType)[0]['text'];
          this.fieldData[iRows].FieldData = '';
        }
      }
    }

    this.fnReset();
  }

  fnCancel() {
    this.fnReset();
  }

  fnCancelAll() {
    this.fnReset();

    this.fieldData = [];
    this.formname = '';
    this.FormMasterId = 0;
  }

  fnSave() {
    if (this.SelectedDepartement === undefined || this.SelectedDepartement === null || this.SelectedDepartement === '') {
      this.toastr.warningToastr('Please select department', 'Alert!');
      return false;
    }

    if (this.formname === undefined || this.formname === null || this.formname === '') {
      this.toastr.warningToastr('Please add form name', 'Alert!');
      return false;
    }

    if (this.fieldData.length === 0) {
      this.toastr.warningToastr('Please add Field Data.', 'Alert!');
      return false;
    }

    // ---------------------- Setting the Sub Field Data Start ------------------------- //
    for (let iRows = 0; iRows < this.fieldData.length; iRows++) {
      let Data = '';
      for (let jRows = 0; jRows < this.SubfieldData.length; jRows++) {
        if (this.SubfieldData[jRows].name === this.fieldData[iRows].FieldName) {
          Data += this.SubfieldData[jRows].data + ',';
        }
      }

      Data = Data.replace(/,\s*$/, '');
      this.fieldData[iRows].FieldData = Data;
    }
    // ---------------------- Setting the Sub Field Data End --------------------------- //

    const datatobeSend = JSON.stringify({
      'masterForm': {
        DepartmentId: this.SelectedDepartement,
        FormMasterId: this.FormMasterId,
        FormName: this.formname
      },
      'formData': this.formData,
      'fieldData': this.fieldData
    });

    this._CustomFormService.SaveData(datatobeSend).subscribe((res) => {
      console.log(res['data']);

      if (res['StatusCode'] === 200) {
        this.toastr.successToastr(res['data'], 'success!');

        this.fnGetData();
        this.fnReset();

        this.fieldData = [];
        this.SubfieldData = [];

        this.formname = '';
        this.FormMasterId = 0;
      } else {
        this.toastr.warningToastr(res['data'], 'Warning!');
      }
    }, (error: any) => { console.log(error); });
  }

  fnEditMaster(id: number) {
    this.fieldData = [];
    this.fnReset();

    for (let iRows = 0; iRows < this.data.length; iRows++) {

      if (this.data[iRows].FormMasterId === id) {
        this.fieldData.push({
          FormMasterId: this.data[iRows].FormMasterId,
          FormDetailId: this.data[iRows].FormDetailId,
          DepartmentId: this.data[iRows].DepartmentId,
          DepartmentName: this.data[iRows].DepartmentName,
          FieldName: this.data[iRows].FieldName,
          FieldType: this.data[iRows].FieldType,
          FieldTypeText: this.data[iRows].FieldTypeText,
          FieldData: this.data[iRows].FieldData,
          Required: this.data[iRows].Required,
          Position: this.data[iRows].Position
        });

        this.FormMasterId = this.data[iRows].FormMasterId;
        this.SelectedDepartement = this.data[iRows].DepartmentId.toString();
        this.formname = this.data[iRows].FormName;
      }
    }
  }

  fnDeleteMaster(id: number) {
    if (confirm('Are you sure ?')) {

      this._CustomFormService.DeleteMaster({ FormMasterId: id }).subscribe((res) => {
        console.log(res['data']);

        if (res['StatusCode'] === 200) {
          this.toastr.successToastr(res['data'], 'success!');

          this.fnGetData();
          this.fnReset();

          this.fieldData = [];
          this.formname = '';
          this.FormMasterId = 0;
        } else {
          this.toastr.warningToastr(res['data'], 'Warning!');
        }
      }, (error: any) => { console.log(error); });
    }
  }

  fnEditDetail(id: number) {
    for (let iRows = 0; iRows < this.fieldData.length; iRows++) {
      if (this.fieldData[iRows].FormDetailId === id) {
        this.fieldname = this.fieldData[iRows].FieldName;
        this.position = this.fieldData[iRows].Position.toString();
        this.required = this.fieldData[iRows].Required.toString();
        this.SelectedDepartement = this.fieldData[iRows].DepartmentId.toString();
        this.SelectedFieldType = this.fieldData[iRows].FieldType.toString();

        this.FormDetailId = this.fieldData[iRows].FormDetailId;
        this.btnAddText = 'Update';

        this.fnGetAbbrivation(this.fieldData[iRows].FieldType.toString());

        this.SubfieldData = [];
        if (this.fieldData[iRows].FieldData.length > 0) {
          for (let jRows = 0; jRows < this.fieldData[iRows].FieldData.split(',').length; jRows++) {
            this.SubfieldData.push({
              name: this.fieldname,
              type: this.fieldData[iRows].FieldType,
              data: this.fieldData[iRows].FieldData.split(',')[jRows]
            });
          }
        }

        break;
      }
    }
  }

  fnDeleteDetail(id: number) {
    if (confirm('Are you sure ?')) {

      this._CustomFormService.DeleteDetail({ FormDetailId: id }).subscribe((res) => {
        console.log(res['data']);

        let Index = 0;
        if (res['StatusCode'] === 200) {
          this.toastr.successToastr(res['data'], 'success!');
          this.fnReset();

          for (let iRows = 0; iRows < this.fieldData.length; iRows++) {
            if (this.fieldData[iRows].FormDetailId === id) {
              Index = iRows;
              break;
            }
          }

          this.fieldData.splice(Index, 1);

        } else {
          this.toastr.warningToastr(res['data'], 'Warning!');
        }
      }, (error: any) => { console.log(error); });
    }
  }

  fnReset() {
    this.fieldname = '';
    this.position = '';
    this.required = '';
    this.SelectedDepartement = '';
    this.SelectedFieldType = '';
    this.btnAddText = 'Add';
    this.FormDetailId = 0;
    this.Abbreviation = '';
    this.SubfieldData = [];
  }

  fnGetAbbrivation(value: any): string {
    let abbreviation = '';

    for (let index = 0; index < this.fieldTypeList.length; index++) {
      if (this.fieldTypeList[index].value === value) {
        abbreviation = this.fieldTypeList[index].abbreviation;
        this.Abbreviation = abbreviation;
        break;
      }
    }
    return abbreviation;
  }

  fnFieldTypeChange(value: any) {
    console.log(value.value);

    this.Abbreviation = '';
    if (value.value !== undefined) {
      const abbreviation = this.fnGetAbbrivation(value.value);
    }
  }

  fnAddFieldData() {
    if (this.fieldname === undefined || this.fieldname === null || this.fieldname === '') {
      this.toastr.warningToastr('Please add field name', 'Alert!');
      return false;
    }

    if (this.SubFieldName === undefined || this.SubFieldName === null || this.SubFieldName === '') {
      this.toastr.warningToastr('Please add Data field', 'Alert!');
      return false;
    }

    if (this.SubfieldData.filter(t => t.name === this.fieldname && t.data === this.SubFieldName).length > 0) {
      this.toastr.errorToastr('Duplicate Field not allowed', 'Alert!');
      return false;
    }

    this.SubfieldData.push({ name: this.fieldname, type: this.SelectedFieldType, data: this.SubFieldName });
    this.SubFieldName = '';
  }

  GetFilteredData(): any {
    return this.SubfieldData.filter(t => t.name === this.fieldname);
  }
}

export interface IDepartment {
  value: string;
  text: string;
}

export interface IDataType {
  value: string;
  text: string;
  abbreviation: string;
}

export interface IModelData {
  output: {};
  recordset: any[];
  recordsets: any[];
  rowsAffected: any[];
}

