/****** Object:  Table [dbo].[FormDetail]    Script Date: 2/13/2019 10:38:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormDetail](
	[FormDetailId] [bigint] IDENTITY(1,1) NOT NULL,
	[FormId] [bigint] NULL,
	[FieldName] [varchar](50) NULL,
	[FieldType] [tinyint] NULL,
	[FIeldData] [varchar](max) NULL,
	[Position] [tinyint] NULL,
	[Required] [tinyint] NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_FormDetail] PRIMARY KEY CLUSTERED 
(
	[FormDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormMaster]    Script Date: 2/13/2019 10:38:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormMaster](
	[FormMasterId] [bigint] IDENTITY(1,1) NOT NULL,
	[DepartmentId] [tinyint] NULL,
	[FormName] [varchar](50) NULL,
	[Status] [tinyint] NULL,
 CONSTRAINT [PK_FormMaster] PRIMARY KEY CLUSTERED 
(
	[FormMasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[FormDetail] ON 

GO
INSERT [dbo].[FormDetail] ([FormDetailId], [FormId], [FieldName], [FieldType], [FIeldData], [Position], [Required], [Status]) VALUES (2, 1, N'Name', 0, NULL, 0, 0, 1)
GO
INSERT [dbo].[FormDetail] ([FormDetailId], [FormId], [FieldName], [FieldType], [FIeldData], [Position], [Required], [Status]) VALUES (3, 1, N'Age', 3, NULL, 1, 1, 1)
GO
INSERT [dbo].[FormDetail] ([FormDetailId], [FormId], [FieldName], [FieldType], [FIeldData], [Position], [Required], [Status]) VALUES (4, 2, N'School', 1, NULL, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[FormDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[FormMaster] ON 

GO
INSERT [dbo].[FormMaster] ([FormMasterId], [DepartmentId], [FormName], [Status]) VALUES (1, 1, N'Form-1', 1)
GO
INSERT [dbo].[FormMaster] ([FormMasterId], [DepartmentId], [FormName], [Status]) VALUES (2, 2, N'Form-2', 1)
GO
SET IDENTITY_INSERT [dbo].[FormMaster] OFF
GO
ALTER TABLE [dbo].[FormDetail] ADD  CONSTRAINT [DF_FormDetail_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[FormMaster] ADD  CONSTRAINT [DF_FormMaster_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[FormDetail]  WITH CHECK ADD  CONSTRAINT [FK_FormDetail_FormMaster] FOREIGN KEY([FormId])
REFERENCES [dbo].[FormMaster] ([FormMasterId])
GO
ALTER TABLE [dbo].[FormDetail] CHECK CONSTRAINT [FK_FormDetail_FormMaster]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Text,
2 = Combo,
3 = Checkbox,
4 = Number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FormDetail', @level2type=N'COLUMN',@level2name=N'FieldType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For Combobox and checkbox data will be save here' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FormDetail', @level2type=N'COLUMN',@level2name=N'FIeldData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Above
1 = Below' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FormDetail', @level2type=N'COLUMN',@level2name=N'Position'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Yes
0 = No' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FormDetail', @level2type=N'COLUMN',@level2name=N'Required'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Active
2 = DeActive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FormDetail', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Active
2 = DeActive' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FormMaster', @level2type=N'COLUMN',@level2name=N'Status'
GO
