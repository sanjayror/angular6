import { Injectable } from '@angular/core';
import { Observable, of, throwError, ErrorObserver } from 'rxjs';
import { mapTo, delay, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpHandler, HttpHeaders } from '@angular/common/http';
import { IModelData } from '../customForm/form-master.component';

@Injectable({
  providedIn: 'root',                 // this provide service in appModule
})
export class CustomFormService {

  baseUrl = 'http://localhost:10001';

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client Side Error : ', errorResponse.error.message);
    } else {
      console.error('Server Side Error : ', errorResponse);
    }

    return throwError('There is problem with the service, We are notified and working on it !!');
  }

  constructor(private _httpClient: HttpClient) {

  }

  GetData(): Observable<IModelData> {
    return this._httpClient.get<IModelData>(`${this.baseUrl}/GetData`).pipe(catchError(this.handleError));
  }

  SaveData(data: any) {
    return this._httpClient.post(`${this.baseUrl}/SaveData`, data, {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
    }).pipe(catchError(this.handleError));
  }

  DeleteMaster(data: any) {
    return this._httpClient.post(`${this.baseUrl}/DeleteMaster`, data, {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
    }).pipe(catchError(this.handleError));
  }

  DeleteDetail(data: any) {
    return this._httpClient.post(`${this.baseUrl}/DeleteDetail`, data, {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
    }).pipe(catchError(this.handleError));
  }
}
