var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var sql = require("mssql");

app.use(bodyParser.json({ limit: '100005MB', type: 'application/json' }));
app.use(bodyParser.urlencoded({ limit: '50000mb', extended: true, parameterLimit: 500000 }));

var dbConfig = {
    user: 'sa',
    password: 'petroit@123',
    server: 'localhost\\SQL2012EXPRESS',
    database: 'sandeep',
    port: 1433
};

var Connection = require('tedious').Connection;
var connection = new Connection(dbConfig);

app.get('/', (req, res) => {
    res.send('Node Server Successfully Running..');
})

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/GetData', (req, res) => {
    sql.connect(dbConfig, (err) => {

        if (err) {
            console.log(err);
        }

        var request = new sql.Request();
        let Query = ` SELECT FormMasterId
                            ,FormName
                            ,FormDetailId
                            ,FieldName 
                            ,FieldType
                            ,DepartmentId
                            ,CASE DepartmentId WHEN '1' THEN 'IT' WHEN '2' THEN 'Sales' WHEN '3' THEN 'HR' END AS DepartmentName
                            ,CASE FieldType WHEN '1' THEN 'Text' WHEN '2' THEN 'Combobox' WHEN '3' THEN 'Checkbox' WHEN '4' THEN 'Number' END AS FieldTypeText
                            ,ISNULL(FieldData,'') AS FieldData
                            ,Position
                            ,CASE Position WHEN '0' THEN 'Above' WHEN '1' THEN 'Below' END AS PositionText
                            ,Required
                            ,CASE Position WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' END AS RequiredText
                    FROM formMaster FM 
                     JOIN FormDetail FD ON FM.FormMasterId = FD.FormId AND FD.Status = 1 
                    WHERE FM.Status = 1 `;

        request.query(Query, (err, recordset) => {
            if (err) {
                console.log(err);
            }

            res.send(recordset);
            sql.close();
        });
    });
});

app.post('/SaveData', (req, res) => {

    var json = {
        masterForm: req.body.masterForm,
        formData: req.body.formData,
        fieldData: req.body.fieldData
    };
    console.log(json);

    var dbConn = new sql.ConnectionPool(dbConfig);
    dbConn.connect().then(() => {

        var requestForm = new sql.Request(dbConn);
        requestForm.query('SELECT COUNT(*) AS COUNT FROM FormMaster WHERE FormName = \'' + req.body.masterForm.FormName + '\' AND Status = 1').then((reForm) => {
            let FormCount = reForm.recordset[0].COUNT;
            if (FormCount > 0) {
                return res.send({ StatusCode: 401, data: "Form Name already exists." });
            }

            var requestInsertMaster = new sql.Request(dbConn);
            let queryMaster = 'INSERT INTO FormMaster (DepartmentId, FormName) VALUES (' + req.body.masterForm.DepartmentId + ', \'' + req.body.masterForm.FormName + '\'); ';

            requestInsertMaster.query(queryMaster)
                .then(() => {

                    var requestSelect = new sql.Request(dbConn);
                    requestSelect.query('SELECT IDENT_CURRENT(\'FormMaster\') AS FormMaseterId').then((re) => {
                        let FormMaseterId = re.recordset[0].FormMaseterId;

                        for (let iRows = 0; iRows < req.body.fieldData.length; iRows++) {
                            let query = 'INSERT INTO FormDetail (FormId, FieldName, FieldType, FIeldData, Position,Required) VALUES  (' + FormMaseterId + ', \'' + req.body.fieldData[iRows].FieldName + '\', ' + req.body.fieldData[iRows].FieldType + ', \'' + req.body.fieldData[iRows].FieldData + '\', ' + req.body.fieldData[iRows].Position + ', ' + req.body.fieldData[iRows].Required + '); ';
                            var requestInsertDetail = new sql.Request(dbConn);
                            requestInsertDetail.query(query)
                                .then(() => {

                                }).catch((err) => {
                                    console.log("Error in Transaction Begin " + err);
                                    dbConn.close();
                                });
                        }
                    });

                    res.send({ StatusCode: 200, data: "Saved Successfully !!!" });

                }).catch((err) => {

                    console.log("Error in Transaction Begin " + err);
                    dbConn.close();
                    res.send(err);
                });
        });

    }).catch((err) => {

        console.log(err);
        res.send(err);
    });
});

app.post('/DeleteMaster', (req, res) => {
    console.log(req.body);

    var dbConn = new sql.ConnectionPool(dbConfig);
    dbConn.connect().then(() => {

        let query = 'UPDATE FormMaster SET Status = 2 WHERE FormMasterId = ' + req.body.FormMasterId + ';';
        query += ' UPDATE FormDetail SET Status = 2 WHERE FormId = ' + req.body.FormMasterId;

        var requestDelete = new sql.Request(dbConn);
        requestDelete.query(query)
            .then(() => {
                res.send({ StatusCode: 200, data: "Deleted Successfully !!!" });
            }).catch((err) => {
                console.log("Error in Transaction Begin " + err);
                dbConn.close();
                res.send({ StatusCode: 404, data: "There is an error" });
            });

    }).catch((err) => {

        console.log(err);
        res.send(err);
    });
});

app.post('/DeleteDetail', (req, res) => {
    console.log(req.body);

    var dbConn = new sql.ConnectionPool(dbConfig);
    dbConn.connect().then(() => {

        let query = 'UPDATE FormDetail SET Status = 2 WHERE FormDetailId = ' + req.body.FormDetailId;

        var requestDelete = new sql.Request(dbConn);
        requestDelete.query(query)
            .then(() => {
                res.send({ StatusCode: 200, data: "Deleted Successfully !!!" });
            }).catch((err) => {
                console.log("Error in Transaction Begin " + err);
                dbConn.close();
                res.send({ StatusCode: 404, data: "There is an error" });
            });

    }).catch((err) => {

        console.log(err);
        res.send(err);
    });
});

app.post('/SaveDatawithtran', (req, res) => {

    var json = {
        masterForm: req.body.masterForm,
        formData: req.body.formData,
        fieldData: req.body.fieldData
    };

    var dbConn = new sql.ConnectionPool(dbConfig);

    dbConn.connect().then(() => {
        var transaction = new sql.Transaction(dbConn);

        transaction.begin().then(() => {
            var requestInsert = new sql.Request(transaction);

            var requestSelect = new sql.Request(transaction);
            requestSelect.query('SELECT TOP 1 * FROM FormMaster').then((re) => { console.log(re); });

            let query = '';
            query += 'INSERT INTO FormMaster (DepartmentId, FormName) VALUES (1, \'FormName\'); ';
            query += 'INSERT INTO FormDetail (FormId, FieldName, FieldType, FIeldData, Position,Required) VALUES  (1, \'abc\', 1, 1, 1, 1); ';

            requestInsert.query(query)
                .then(() => {
                    transaction.commit().then((recordSet) => {

                        console.log('Inserted !!!!');
                        dbConn.close();
                        res.send({ data: "Inserted Successfully !!!" });
                    }).catch((err) => {

                        console.log("Error in Transaction Commit " + err);
                        dbConn.close();
                        res.send(err);
                    });
                }).catch((err) => {

                    console.log("Error in Transaction Begin " + err);
                    dbConn.close();
                    res.send(err);
                });

        }).catch((err) => {

            console.log(err);
            dbConn.close();
            res.send(err);
        });
    }).catch((err) => {

        console.log(err);
        res.send(err);
    });
});

app.get('/put', function (req, res) {
    var dbConn = new sql.ConnectionPool(dbConfig);

    dbConn.connect().then(function () {
        var transaction = new sql.Transaction(dbConn);

        transaction.begin().then(function () {
            var request = new sql.Request(transaction);

            request.query("Insert into ac (ID) values (123)")
                .then(function () {
                    transaction.commit().then(function (recordSet) {

                        console.log('Inserted !!!!');
                        dbConn.close();
                        res.send({ data: "Inserted Successfully !!!" });
                    }).catch(function (err) {

                        console.log("Error in Transaction Commit " + err);
                        dbConn.close();
                        res.send(err);
                    });
                }).catch(function (err) {

                    console.log("Error in Transaction Begin " + err);
                    dbConn.close();
                    res.send(err);
                });

        }).catch(function (err) {

            console.log(err);
            dbConn.close();
            res.send(err);
        });
    }).catch(function (err) {

        console.log(err);
        res.send(err);
    });
});

var server = app.listen(10001, () => {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Server is running at : %s", port)
});
