import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-bootstrap-popup',
  templateUrl: './bootstrap-popup.component.html',
  styleUrls: ['./bootstrap-popup.component.css']
})
export class BootstrapPopupComponent implements OnInit {
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  progress = 0;

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.dropdownList = [
      { item_id: 1, item_text: 'Mumbai' },
      { item_id: 2, item_text: 'Bangaluru' },
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' },
      { item_id: 5, item_text: 'New Delhi' }
    ];

    for (let index = 6; index < 100; index++) {
      this.dropdownList.push({ item_id: index, item_text: 'Mumbai ' + index });
    }

    this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

    async function delay(ms: number) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    // --------------------------------- angular-progress-bar ----------------------------------------------
    (async () => {
      // Do something before delay
      console.log('before delay');

      for (let index = 0; index < 100; index++) {
        await delay(100);
        this.progress = this.progress + 1;
      }
      // Do something after
      console.log('after delay');
    })();
  }
  // --------------------------------- angular-progress-bar ----------------------------------------------

  fnLoadingBar() {
    /** spinner starts on init */
    this.spinner.show();

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 3000);
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
}
