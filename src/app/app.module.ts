import { BrowserModule } from '@angular/platform-browser'; // BrowserModule also re-exports CommonModule
import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common'; // (No need of this)
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ChartComponent } from './chart/chart.component';
import { Chartv2Component } from './chartv2/chartv2.component';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { AngularToasterComponent } from './AngularToaster/angular-toaster.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BootstrapPopupComponent } from './popup/bootstrap-popup.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ChartsModule } from 'ng2-charts';

// for paging
import { NgbdPaginationConfig } from './shared/pagination-config';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormMasterComponent } from './customForm/form-master.component';
import { AngularMaterial } from './app.common';

// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';
// Import FusionCharts library
import * as FusionCharts from 'fusioncharts';
// Load FusionCharts Individual Charts
import * as Charts from 'fusioncharts/fusioncharts.charts';
// Use fcRoot function to inject FusionCharts library, and the modules you want to use
FusionChartsModule.fcRoot(FusionCharts, Charts);

// --------------------------------- gauge ----------------------------------------------
// npm install --save ngx-gauge
import { NgxGaugeModule } from 'ngx-gauge';
// --------------------------------- gauge ----------------------------------------------

// --------------------------------- angular-gauge-chart ----------------------------------------------
// npm install --save angular-gauge-chart
import { GaugeChartComponent } from 'angular-gauge-chart';
// --------------------------------- angular-gauge-chart ----------------------------------------------

// --------------------------------- angular-progress-bar ----------------------------------------------
// npm install --save angular-progress-bar
import {ProgressBarModule} from 'angular-progress-bar';
// --------------------------------- angular-progress-bar ----------------------------------------------

// --------------------------------- ngx-spinner -------------------------------------------------------
// npm install --save ngx-spinner
import { NgxSpinnerModule } from 'ngx-spinner';
import { HomeComponent } from './home.component';
import { PageNotFoundComponent } from './page-not-found.component';
// import { EmployeeModule } from './employee/employee.module';
import { ToastrComponent } from './toastr/toastr.component';
// --------------------------------- ngx-spinner -------------------------------------------------------
import { SharedModule } from './shared/shared.module';
import { Ng6featuresComponent } from './ng6features/ng6features.component';
import { DirColorDirective } from './dir-color.directive';
import { ColorComponent } from './ng6features/ColorComponent';
import { NumberParentComponent } from './ng6features/number-parent.component';
import { NumberComponent } from './ng6features/number.component';
@NgModule({
  declarations: [
    AppComponent,
    // AngularToasterComponent,
    BootstrapPopupComponent,
    NgbdPaginationConfig,
    FormMasterComponent,
    ChartComponent,
    Chartv2Component,
    GaugeChartComponent,
    HomeComponent,
    PageNotFoundComponent,
    ToastrComponent,
    Ng6featuresComponent,
    DirColorDirective,
    ColorComponent,
    NumberParentComponent,
    NumberComponent,
  ],
  imports: [
    BrowserModule,
    // EmployeeModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule,
    NgbModule,
    HttpClientModule,
    AngularMaterial,
    ChartsModule,
    FusionChartsModule, // Include in imports,
    NgxGaugeModule,
    ProgressBarModule,
    NgxSpinnerModule,
    SharedModule,
  ],
  exports: [
    AngularMaterial,
    ColorComponent
  ],
  entryComponents: [ColorComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
