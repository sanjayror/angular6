import { Component, OnInit } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-angular-toaster',
  templateUrl: './angular-toaster.component.html',
  styleUrls: ['./angular-toaster.component.css']
})
export class AngularToasterComponent implements OnInit {

  constructor(public toastr: ToastrManager) { }

  ngOnInit() {

  }

  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
  }

  showError() {
    this.toastr.errorToastr('This is error toast.', 'Oops!');
  }

  showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
  }

  showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
  }

  showCustom() {
    this.toastr.customToastr('Custom Toast', 'this is custom', { enableHTML: true });
  }

  showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', { position: position });
  }
}
