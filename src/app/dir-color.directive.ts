import { Directive, ElementRef, HostListener, Input, Output, OnInit, EventEmitter, AfterViewInit } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[dynamicColor]'
})
export class DirColorDirective implements OnInit, AfterViewInit {

  constructor(private elRef: ElementRef) {
    console.log('constructor : ' + this.dynamicColor);
  }
  // tslint:disable-next-line:no-input-rename
  @Input('dynamicColor') dynamicColor: string;
  @Input() defaultValue: string;

  Counter = 0;
  @Output() valueChange = new EventEmitter();
  // tslint:disable-next-line:no-output-rename
  @Output('dynamicColor') customSubmit: EventEmitter<any> = new EventEmitter();

  @HostListener('submit', ['$event'])
  onSubmit(e) {
    e.preventDefault();
    console.log('call this');
    this.customSubmit.emit(e);
  }

  @HostListener('click', ['$event', 'a'])
  onCall(e, a) {
    // e.preventDefault();
    console.log('onCall this' + this.dynamicColor);
    this.customSubmit.emit(e);
    this.elRef.nativeElement.style.backgroundColor = 'green';
  }

  valueChanged() {
    this.Counter = this.Counter + 1;
    this.valueChange.emit(this.Counter);
  }

  public colorChanger(color: string) {
    this.elRef.nativeElement.style.backgroundColor = color;
  }

  ngAfterViewInit(): void {
    this.elRef.nativeElement.style.backgroundColor = this.dynamicColor;
  }

  ngOnInit() {
    console.log('ngOnInit : ' + this.dynamicColor);
  }

  @HostListener('mouseover') onmouseover() {
    console.log('mouseover : ' + this.dynamicColor);
    // this.changeBackgroundColor(this.dynamicColor || this.defaultValue);
  }

  @HostListener('mouseleave') onMouseLeave() {
    // this.changeBackgroundColor('white');
  }

  private changeBackgroundColor(color: string) {
    this.elRef.nativeElement.style.backgroundColor = color;
  }

  @HostListener('onchange') onchange() {
    console.log('onchange : ' + this.dynamicColor);
  }
}
