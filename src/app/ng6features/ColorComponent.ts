import { Input, OnInit, Component, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-color',
  template: `this is color - {{color}}
              <p [ngStyle]= "{'background-color': 'lime'}">ok</p>
              <div [style.background-color]='color'>Color Component</div>
              <input type="text" [formControl]="searchControl" placeholder="search">
            `,
})
export class ColorComponent implements OnInit, AfterViewInit {
  @Input()
  color: string;
  searchControl: FormControl;
  debounce = 400;
  constructor() {
    console.log('Color : ' + this.color);
  }
  ngOnInit() {
    console.log('Color : ' + this.color);
    this.searchControl = new FormControl('');
    this.searchControl.valueChanges.pipe(debounceTime(this.debounce), distinctUntilChanged())
      .subscribe(query => {
        console.log(query);
      });
  }
  ngAfterViewInit() {
    console.log('AfterViewInit : ' + this.color);
  }
}
