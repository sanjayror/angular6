import { ColorComponent } from './ColorComponent';
import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-ng6features',
  templateUrl: './ng6features.component.html',
  styleUrls: ['./ng6features.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class Ng6featuresComponent implements OnInit, AfterViewInit {
  lessons = false;
  shouldSayHello = false;
  myColor = 'yellow';
  colors = ['cyan', 'green', 'yellow'];
  message = '';

  @ViewChild(ColorComponent)
  primarySampleComponent: ColorComponent;

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    console.log('Values on ngAfterViewInit():');
    console.log('primarySampleComponent: ', this.primarySampleComponent.color);
  }

  displayCounter(count) {
    console.log(count);
  }

  onSubmit(e) {
    console.log(e);
    this.message = 'submitted';
  }

  onCall(e) {
    console.log(e);
    this.message = 'submitted';
  }

  dataChanged(newObj) {
    console.log('dataChanged : ' + newObj);
    // here comes the object as parameter
  }

  changed(e) {
    console.log('changed : ' + e.target.value);
    // event comes as parameter, you'll have to find selectedData manually
    // by using e.target.data
  }

}
