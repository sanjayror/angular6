import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Chart } from 'chart.js';

export interface Data {
  month: String;
  price: Number;
}

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent implements OnInit, AfterViewInit {
  // https://appdividend.com/2018/09/12/angular-charts-example-tutorial/

  // npm install ng2-charts --save
  // npm install chart.js --save
  // https://valor-software.com/ng2-charts/

  constructor(private elementRef: ElementRef) { }
  title = 'app';
  data: Data[];
  month = [];
  price = [];
  chart = [];

  colors = [
    { // 1st Year.
      backgroundColor: 'rgba(77,83,96,0.2)'
    },
    { // 2nd Year.
      backgroundColor: 'rgba(30, 169, 224, 0.8)'
    }
  ];

  json = {
    'results': [
      {
        'month': 'Jan',
        'price': '180'
      },
      {
        'month': 'Feb',
        'price': '200'
      },
      {
        'month': 'March',
        'price': '210'
      },
      {
        'month': 'April',
        'price': '190'
      },
      {
        'month': 'May',
        'price': '240'
      },
      {
        'month': 'June',
        'price': '230'
      },
      {
        'month': 'July',
        'price': '260'
      },
      {
        'month': 'Aug',
        'price': '210'
      },
      {
        'month': 'Sept',
        'price': '300'
      }]
  };

  @ViewChild('myCanvas') myCanvas: ElementRef;
  public context: CanvasRenderingContext2D;


  // --------------------------------------------------------------------------------------------------//
  // lineChart
  // tslint:disable-next-line:member-ordering
  public lineChartData: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C' }
  ];
  // tslint:disable-next-line:member-ordering
  public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  // tslint:disable-next-line:member-ordering
  public lineChartOptions: any = {
    responsive: true
  };
  // tslint:disable-next-line:member-ordering
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  // tslint:disable-next-line:member-ordering
  public lineChartLegend = true;
  public lineChartType = 'line';
  // --------------------------------------------------------------------------------------------------------------//

  // --------------------------------------------------------------------------------------------------------------//
  // tslint:disable-next-line:member-ordering
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  // tslint:disable-next-line:member-ordering
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  // tslint:disable-next-line:member-ordering
  public barChartType = 'bar';
  // tslint:disable-next-line:member-ordering
  public barChartLegend = true;

  public barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];
  // --------------------------------------------------------------------------------------------------------------//


  // --------------------------------------------------------------------------------------------------------------//
  // Doughnut
  // tslint:disable-next-line:member-ordering
  public doughnutChartLabels = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  // tslint:disable-next-line:member-ordering
  public doughnutChartData = [350, 450, 100];
  public doughnutChartType = 'doughnut';
  // --------------------------------------------------------------------------------------------------------------//

  // --------------------------------------------------------------------------------------------------------------//
  // Radar
  public radarChartLabels: string[] = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];

  public radarChartData: any = [
    { data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B' }
  ];
  public radarChartType = 'radar';
  // --------------------------------------------------------------------------------------------------------------//

  // --------------------------------------------------------------------------------------------------------------//
  // Pie
  public pieChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
  public pieChartData: number[] = [300, 500, 100];
  public pieChartType = 'pie';
  // --------------------------------------------------------------------------------------------------------------//

  // --------------------------------------------------------------------------------------------------------------//
  public randomizeType(): void {
    this.lineChartType = this.lineChartType === 'line' ? 'bar' : 'line';
    this.pieChartType = this.pieChartType === 'doughnut' ? 'pie' : 'doughnut';
  }
  // --------------------------------------------------------------------------------------------------------------//


  // --------------------------------------------------------------------------------------------------------------//
  // PolarArea
  // tslint:disable-next-line:member-ordering
  public polarAreaChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales'];
  // tslint:disable-next-line:member-ordering
  public polarAreaChartData: number[] = [300, 500, 100, 40, 120];
  // tslint:disable-next-line:member-ordering
  public polarAreaLegend = true;

  // tslint:disable-next-line:member-ordering
  public polarAreaChartType = 'polarArea';
  // --------------------------------------------------------------------------------------------------------------//


  ngOnInit() { }

  ngAfterViewInit() {

    setTimeout(() => {

      this.json.results.forEach(y => {
        this.month.push(y.month);
        this.price.push(y.price);
      });

      const ctx = document.getElementById('myCanvas');
      const htmlRef = this.elementRef.nativeElement.querySelector(`#myCanvas`);

      this.chart = new Chart(htmlRef, {
        type: 'line',
        data: {
          labels: this.month,
          datasets: [
            {
              data: this.price,
              borderColor: '#3cba9f',
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    });
  }

  public randomize(): void {
    const _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = { data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label };
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  // --------------------------------------------------------------------------------------------------------------//
}




